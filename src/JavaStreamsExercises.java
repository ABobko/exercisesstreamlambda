import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/*
1.Tworzymy klasę Person. Tworzysz listę z przykładowymi osobami (min 7),
osoba powinna składac sie z wieku, imienia i nazwiska. (edited)
Napisz program, który zgrupuje wszystkich ludzi po nazwiskach.
Wypisz do konsoli, ktore nazwisko jest najpopularniejsze


 Użyj strumieni
2.  Napisz program, który wyliczy średnią wartość wieku wszystkich ludzi
3.  Napisz program, który znajdzie najstarszego człowieka i dopisz do jego imienia prefix "Mędrzec".
4.  Wyobraź sobie, że tworzymy nadczłowieka. To człowiek składający się z wszystkich innych ludzi,
    jego imię tworzą pierwsze litery innych ludzi, jego nazwisko tworzą ostatnie litery innych ludzi,
    jego wiek to suma lat całej reszty ludzi (edited)

5.  Stwórz listę lat ludzi (List<Integer>) na podstawie listy ludzi

6.  Znajdź człowieka, którego suma cyfr lat jest taka sama jak suma liter w imieniu
7.  Posortuj ludzi po wieku od najstarszego do najmłodszego
8.  Usuń ludzi ze zbioru, którzy występują podwójnie (są zdublowani).
    Następnie podaj ile takich ludzi było. (istnieje do tego metoda, przeszukaj dokumentacje)
9.  Utwórz klasę Animal, z polami wiek i imię. Na podstawie zbioru ludzi,
    utwórz zbiór zwierząt. Zwierzę przejmuje imię po złączeniu imienia i nazwiska człowieka,
    a wiek zwierzęcia to wiek człowieka podzielony przez 10
10. Zamień wiek człowieka na psie lata, (n * 6 - 2), wyświetl tych ludzi,
    których wiek przekracza 50 lat po zmianie
11. Zgrupuj ludzi, których suma liter w imieniu i nazwisku jest taka sama

*/

public class JavaStreamsExercises {
    public static void main(String[] args) {
        DataFeeder feeder = new DataFeeder();
        List<Person> data = feeder.getData();

        List<Person> resultSet;


        //ex.1
        System.out.println("------ex.1------");

        resultSet = data.stream()
                .sorted(Comparator.comparing(Person::getSurname))
                .collect(Collectors.toList());

        resultSet.forEach(System.out::println);


        //ex.2
        System.out.println("------ex.2------");

         data.stream()
                .mapToInt(Person::getAge)
                .average()
                .ifPresent(System.out::println);


        //ex.3
        System.out.println("------ex.3------");

        data.stream()
                .max(Comparator.comparing(Person::getAge))
                .ifPresent(p->p.setName("Mędrzec "+p.getName()));
        data.forEach(System.out::println);


        //ex.4
        System.out.println("------ex.4------");

        Person someKindOfASuperMan = data.stream()
                .reduce(new Person(" "," ",0)
                        ,(p1,p2)->mutatePerson(p1,p2));

        System.out.println("ex.4 result: " +someKindOfASuperMan);


        //ex.5
        System.out.println("------ex.5------");

        List<Integer> ageList = data.stream()
                .map(p->p.getAge())
                .collect(toList());

        //ex.6
        System.out.println("------ex.6------");

        data.stream()
        .filter(p -> checkAgeAndName(p))
        .forEach(System.out::println);

        //ex.7
        System.out.println("------ex.7------");

        List<Person> sortedByAge = data.stream()
                .sorted(Comparator.comparing(Person::getAge))
                .collect(toList());

        sortedByAge.forEach(System.out::println);

        //ex.8
        System.out.println("------ex.8------");

        List<Person> duplicates = data.stream()
                .filter(p -> Collections.frequency(data, p) > 1)
                .collect(toList());

        List<Person> distinctElem = data.stream()
                .distinct()
                .collect(toList());

        System.out.println("overall list size: "+data.size());
        System.out.println("distinct elements: " +distinctElem.size());
        System.out.println("number of duplicates: "
                +(data.size()-distinctElem.size()));
        System.out.println("nonunique occurences: "+duplicates.size());

        //ex.9
        System.out.println("------ex.9------");

        List<Animal> animalList = data.stream()
                .map(p -> animalTransform(p))
                .collect(toList());

        animalList.forEach(System.out::println);

        //ex.10
        System.out.println("------ex.10------");

        List<Person> dogAgeConverted = data.stream()
                .map(p -> dogYersConversion(p))
                .collect(toList());

        dogAgeConverted.stream()
                .filter(p -> p.getAge()>50)
                .forEach(System.out::println);

        //ex.11
        System.out.println("------ex.11------");

        List<Person> sameNameAndSurnameLength = data.stream()
                .filter(p->p.getName().length()
                        ==p.getSurname().length())
                .collect(toList());

                sameNameAndSurnameLength.forEach(System.out::println);

    }

    private static Person mutatePerson(Person person1, Person person2){
        String name =person1.getName();
        String surname;
        int age = person1.getAge();

        name = name + person2.getName().charAt(0);
        surname = person1.getSurname() + person2.getSurname()
                .charAt(person2
                        .getSurname()
                        .length()-1);
        age+= person2.getAge();

        return new Person(name,surname,age);
    }

    private static boolean checkAgeAndName(Person person){
        int nameLength = person.getName().length();
        int age = person.getAge();
        int sumOfDigits = 0;

        while(!(age%10 == 0)){
            sumOfDigits +=age%10;
            age = (age - age%10)/10;
        }

        return nameLength == sumOfDigits;

    }

    private static Animal animalTransform(Person person){
        String animalName = person.getName() + person.getSurname();
        int animalAge = person.getAge()/10;
        if(animalAge>0)
            return new Animal(animalName, animalAge);
        return new Animal(animalName, 1);
    }

    private static Person dogYersConversion(Person person){
        int dogAge = person.getAge()*6-2;
        return new Person(person.getName(), person.getSurname(), dogAge);
    }
    
}
