import java.util.ArrayList;
import java.util.List;

class DataFeeder {
    private List<Person> data;

    DataFeeder(){
        data = new ArrayList<>();
        data.add(new Person("Bob","Seger", 73));
        data.add(new Person("Ann","Wilson", 68));
        data.add(new Person("Alice","Cooper", 70));
        data.add(new Person("Steve","Perry", 69 ));
        data.add(new Person("Kelly", "Hansen", 57));
        data.add(new Person("Steve", "Perry", 69));
        data.add(new Person("Elton","John", 71));
        data.add(new Person("Bruce", "Springsteen", 69));
        data.add(new Person("Eric","Clapton", 73));
        data.add(new Person("Dave", "Bickler", 65));
        data.add(new Person("Billy", "Gibbons", 68));
        data.add(new Person("Neil","Young", 72));
        data.add(new Person("Jimmy","Page", 74));
        data.add(new Person("notarockstar", "notatall", 66));
        data.add(new Person("duplicate", "duplicate", 53));
        data.add(new Person("duplicate", "duplicate", 53));
        data.add(new Person("young", "prodigy", 6));
        data.add(new Person("little", "baby", 2));

    }

    List<Person> getData(){
        return data;
    }
}
