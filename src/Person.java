public class Person {
    private int age;
    private String name;
    private String surname;

    Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    int getAge() {
        return age;
    }

    String getName() {
        return name;
    }

    String getSurname() {
        return surname;
    }

    void setName(String name){
        this.name = name;
    }


    /**
     *  IDE generated
     *  Overriden methods:
     *      - toString()
     *      - equals()
     *      - hashCode()
     */

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return age == person.age
                && (name != null ? name.equals(person.name) : person.name == null)
                && (surname != null ? surname.equals(person.surname) : person.surname == null);
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }
}
